﻿using System;
using TechTalk.SpecFlow;

namespace StackOverflow.Specs
{
    [Binding]
    public class CalculatorAdditionSteps : Steps
    {
        [Given(@"I have cleared any previous result")]
        public void GivenIHaveClearedAnyPreviousResult()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"I enter (.*) into the calculator")]
        public void WhenIEnterIntoTheCalculator(int p0)
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"I choose addition")]
        public void WhenIChooseAddition()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"I add (.*) and (.*) together")]
        public void WhenIAddAnd(int p0, int p1)
        {
            When(string.Format("I enter {0} into the calculator"));
            When("I choose addition");
            When(string.Format("I enter {0} into the calculator"));
        }

        [Then(@"the result should be (.*)")]
        public void ThenTheResultShouldBe(int p0)
        {
            ScenarioContext.Current.Pending();
        }
    }
}
