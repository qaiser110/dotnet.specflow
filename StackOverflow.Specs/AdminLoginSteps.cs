﻿using System;
using TechTalk.SpecFlow;

namespace StackOverflow.Specs
{
    [Binding]
    class AdminLoginSteps
    {
        private AdminLoginDetails _adminLoginDetails;

        // constructor used for injecting AdminLoginDetails
        public AdminLoginSteps(AdminLoginDetails adminLoginDetails)
        {
            _adminLoginDetails = adminLoginDetails;
        }
        
        [Given(@"I have logged in as administrator '(.*)'")]
        public void GivenIHaveLoggedInAsAdministrator(string p0)
        {
            // store the UserName for later use
            _adminLoginDetails.UserName = p0;
        }
        
        [When(@"I navigate to the user profile page")]
        public void WhenINavigateToTheUserProfilePage()
        {
            // ScenarioContext.Current.Pending();
        }
        
    }
}
