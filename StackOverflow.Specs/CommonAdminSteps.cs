﻿using TechTalk.SpecFlow;

namespace StackOverflow.Specs
{
    [Binding]
    class CommonAdminSteps
    {
        private AdminLoginDetails _adminLoginDetails;

        // constructor used for injecting AdminLoginDetails
        public CommonAdminSteps(AdminLoginDetails adminLoginDetails)
        {
            _adminLoginDetails = adminLoginDetails;
        }
        
        [Then(@"the correct username should be displayed")]
        public void ThenTheCorrectUsernameShouldBeDisplayed()
        {
            // retrieve the UserName from "AdminLoginDetails" object
            string uName = _adminLoginDetails.UserName;
            // use uName for assertion
        }
    }
}
