﻿using System;

namespace StackOverflow.Specs
{
    // This class will be injected in our tests
    public class AdminLoginDetails
    {
        public string UserName { get; set; }
    }
}
