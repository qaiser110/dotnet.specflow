﻿using System;
using TechTalk.SpecFlow;

namespace StackOverflow.Specs
{
    [Binding]
    public class ExternalBindingAssembliesSteps
    {
        [When(@"I submit a status update of ""(.*)""")]
        public void WhenISubmitAStatusUpdateOf(string p0)
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"the update should appear in my timeline")]
        public void ThenTheUpdateShouldAppearInMyTimeline()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
