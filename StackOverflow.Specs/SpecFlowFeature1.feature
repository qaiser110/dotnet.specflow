﻿Feature: Ordering answers

@votes
Scenario: The answer with highest votes gets to the top
	Given There is a question "What's your favorite color?" with answers:
		| Answer	| Vote	|
		| Red		| 1		|
		| Green		| 1		|
	When you upvote answer "green"
	Then the answer "green" should be on top