﻿using System;
using TechTalk.SpecFlow;

namespace Foo.Common.Specs
{
    [Binding]
    public class SystemUserSteps
    {
        [Given(@"The user '(.*)' exists in the system")]
        public void GivenTheUserExistsInTheSystem(string p0)
        {
            ScenarioContext.Current.Pending();
        }
    }
}